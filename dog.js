class Dog {
  bark() {
    return 'Bark';
  }

  jump() {
    return 'Jump';
  }
}

module.exports = { Dog }
